//
//  ViewController.swift
//  G57L4
//
//  Created by Max on 05.10.17.
//  Copyright © 2017 Max. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var inputTextField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        WorkClass.printHelloWorld()
        WorkClass.countHelloWorld(count: 6)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let number = Int(inputTextField.text!)!
        print(number)
        let intNum = Int(number)
        WorkClass.countHelloWorld(count: number)
        
        
    }
    
    
}

